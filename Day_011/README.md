# 玩轉 Python 與 MongoDB_Day11_基本判斷大小邏輯

今天我們要來教學基本的 Mongo 邏輯判斷大小的語法，同樣會用到之前插入的資料集以及資料模型，忘記模型的人可以參考
[這個連結](https://github.com/nickchen1998/2023_ithelp_marathon/blob/main/Day_007/schema.py)

今天主要會教學的內容如下：

- $gt 查詢條件
- $gte 查詢條件
- $lt 查詢條件
- $lte 查詢條件
- $ne 查詢條件

## 一、大於 $gt

> 語法：collection.find_one({“欄位名稱”: {"$gt": "數值“}})

可以看到下方的範例當中，透過大於的語法，針對 statistics 底下的 A2_amount 欄位，並且數值要大於 5，
同時也把查詢到資料設定為只顯示 statistics 欄位

```python
print("---- demo gt ----")
result = collection.find_one(
    {"statistics.A2_amount": {"$gt": 5}},
    {"statistics": 1}
)
print(result)
```

可以看到執行結果當中找到的資料 A2_amount 這個欄位的數值的確是大於 5 的

![gt 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_011/img/gt%20%E6%88%AA%E5%9C%96.jpg?raw=true)

## 二、大於等於 $gte

> 語法：collection.find_one({“欄位名稱”: {"$gte": "數值“}})

可以看到下方的範例當中，透過大於等於的語法，針對 statistics 底下的 A2_amount 欄位，並且數值要大於等於 1，
同時也把查詢到資料設定為只顯示 statistics 欄位，為了展示方便，也把資料做了排序，針對 A2_amount 做升冪排序

```python
print("---- demo gte ----")
result = collection.find_one(
    {"statistics.A2_amount": {"$gte": 1}},
    {"statistics": 1},
    sort=[("statistics.A2_amount", ASCENDING)]
)
print(result)
```

可以看到執行結果當中找到的資料 A2_amount 這個欄位的數值的確是大於等於 1 的

![gte 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_011/img/gte%20%E6%88%AA%E5%9C%96.jpg?raw=true)

## 三、小於 $lt

> 語法：collection.find_one({“欄位名稱”: {"$lt": "數值“}})

可以看到下方的範例當中，透過小於的語法，針對 statistics 底下的 A2_amount 欄位，並且數值要小於 5，
同時也把查詢到資料設定為只顯示 statistics 欄位

```python
print("---- demo lt ----")
result = collection.find_one(
    {"statistics.A2_amount": {"$lt": 5}},
    {"statistics": 1}
)
print(result)
```

可以看到執行結果當中找到的資料 A2_amount 這個欄位的數值的確是小於 5 的

![lt 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_011/img/lt%20%E6%88%AA%E5%9C%96.jpg?raw=true)

## 四、小於等於 $lte

> 語法：collection.find_one({“欄位名稱”: {"$lte": "數值“}})

可以看到下方的範例當中，透過小於等於的語法，針對 statistics 底下的 A2_amount 欄位，並且數值要小於等於 5，
同時也把查詢到資料設定為只顯示 statistics 欄位，同樣為了展示方便，也設定了針對 statistics.A2_amount 欄位做降冪排序

```python
print("---- demo lte ----")
result = collection.find_one(
    {"statistics.A2_amount": {"$lte": 5}},
    {"statistics": 1},
    sort=[("statistics.A2_amount", DESCENDING)]
)
print(result)
```

可以看到執行結果當中找到的資料 A2_amount 這個欄位的數值的確是小於等於 5 的

![lte 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_011/img/lte%20%E6%88%AA%E5%9C%96.jpg?raw=true)

## 五、不等於 $ne

> 語法：collection.find_one({“欄位名稱”: {"$ne": "數值“}})

可以看到下方的範例當中，透過不等於的語法，針對 statistics 底下的 A2_amount 欄位，並且數值要不等於 1，
同時也把查詢到資料設定為只顯示 statistics 欄位

```python
print("---- demo ne ----")
result = collection.find_one(
    {"statistics.A2_amount": {"$ne": 1}},
    {"statistics": 1}
)
print(result)
```

可以看到執行結果當中找到的資料 A2_amount 這個欄位的數值的確是不等於 1 的

![ne 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_011/img/ne%20%E6%88%AA%E5%9C%96.jpg?raw=true)