import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from pymongo import ASCENDING, DESCENDING

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

# demo gt
# print("---- demo gt ----")
# result = collection.find_one(
#     {"statistics.A2_amount": {"$gt": 5}},
#     {"statistics": 1}
# )
# print(result)

# demo gte
# print("---- demo gte ----")
# result = collection.find_one(
#     {"statistics.A2_amount": {"$gte": 1}},
#     {"statistics": 1},
#     sort=[("statistics.A2_amount", ASCENDING)]
# )
# print(result)

# demo lt
# print("---- demo lt ----")
# result = collection.find_one(
#     {"statistics.A2_amount": {"$lt": 5}},
#     {"statistics": 1}
# )
# print(result)

# demo lte
# print("---- demo lte ----")
# result = collection.find_one(
#     {"statistics.A2_amount": {"$lte": 5}},
#     {"statistics": 1},
#     sort=[("statistics.A2_amount", DESCENDING)]
# )
# print(result)

# demo ne
print("---- demo ne ----")
result = collection.find_one(
    {"statistics.A2_amount": {"$ne": 1}},
    {"statistics": 1}
)
print(result)

client.close()
