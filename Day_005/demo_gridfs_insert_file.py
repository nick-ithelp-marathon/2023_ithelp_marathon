import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from gridfs import GridFS
from pymongo.mongo_client import MongoClient

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")

# 建立 GridFS 物件
gridfs = GridFS(database=database)

with open(file="範例圖片.png", mode="rb") as file:
    inserted_id = gridfs.put(data=file)
    print(inserted_id)

client.close()
