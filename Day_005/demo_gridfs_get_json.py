import os
import json
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from gridfs import GridFS
from pymongo.mongo_client import MongoClient
from bson.objectid import ObjectId

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")

# 建立 GridFS 物件
gridfs = GridFS(database=database)

# 透過 id 取得所有被切割的檔案，記得 id 要使用 bson.object_id.ObjectId
chunks = gridfs.get(file_id=ObjectId("646389db4f978885e6985b93"))

# 將 chunks 進行重組
result = b""
for chunk in chunks:
    result += chunk

print(json.loads(result.decode("utf8")))

client.close()
