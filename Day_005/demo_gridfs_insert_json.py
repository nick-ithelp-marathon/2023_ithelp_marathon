import os
import json
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from gridfs import GridFS
from pymongo.mongo_client import MongoClient

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 讀取 datas 目錄下的 1 月份資料
file = BASE_DIR / "datas" / "臺中市111年1月10大易肇事路口.json"

# 建立 client 並與 db 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")

# 建立 GridFS 物件
gridfs = GridFS(database=database)

with open(file=file, encoding="utf-8") as f:
    # 讀取 json 檔案的內容
    datas = json.load(fp=f)

    # 將 dictionary 轉換成字串，並指定字串編碼
    string_datas = json.dumps(datas).encode("utf-8")

    # 進行寫入
    inserted_id = gridfs.put(data=string_datas)
    print(inserted_id)

client.close()
