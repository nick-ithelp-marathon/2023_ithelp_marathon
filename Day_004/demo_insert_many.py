import os
import json
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient


# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 讀取 datas 目錄下的 1 月份資料
file = BASE_DIR / "datas" / "臺中市111年1月10大易肇事路口.json"

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="HighRiskIntersection")
collection = Collection(
    database=database,
    name="Intersection"
)
# 插入多筆資料
with open(file=file, encoding="utf-8") as f:
    datas = json.load(fp=f)

    inserted_ids = collection.insert_many(
        documents=datas
    ).inserted_ids

    print(inserted_ids)


client.close()
