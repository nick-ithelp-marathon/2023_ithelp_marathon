# 玩轉 Python 與 MongoDB_Day04_資料插入

今天我們會介紹該如何在 mongodb 當中進行資料的寫入，總共會分為三大項，分別為插入單筆資料、插入多筆資料以及 WriteConcern

**注意：在 MongoDB 中，只需要設定好資料庫以及 Collection 的名稱後，寫入資料時系統會自動替你建立資料庫以及資料表**

**注意：程式碼結尾記得使用 client.close() 將連線關閉**

## 一、寫入單筆資料

我們可以透過直接呼叫 `Collection` 物件下的 `insert_one` 方法直接進行單筆資料的寫入，針對下方的範例進行解釋：

- 開啟指定的檔案並透過 json 套件進行讀取
- 利用上述語法進行資料插入
- 於資料插入後取得 inserted_id，即會取得該筆資料插入進資料庫的 id
- 使用 print 印出 id

**insert_one() 方法只接收 dictionary 型態的資料**

```python
import os
import json
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 讀取 datas 目錄下的 1 月份資料
file = BASE_DIR / "datas" / "臺中市111年1月10大易肇事路口.json"

# 設定連線資訊
# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(
    database=database,
    name="day004"
)
# 插入單筆資料
with open(file=file, encoding="utf-8") as f:
    datas = json.load(fp=f)

    inserted_id = collection.insert_one(
        document=datas[0]
    ).inserted_id

    print(inserted_id)

client.close()
```

下方圖片中我們可以透過 MongoCompass 查看插入資料後的結果
![插入單筆資料結果](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_004/img/%E6%8F%92%E5%85%A5%E5%96%AE%E7%AD%86%E8%B3%87%E6%96%99%E7%B5%90%E6%9E%9C.jpg?raw=true)

## 二、插入多筆資料

我們可以透過直接呼叫 `Collection` 物件下的 `insert_many` 方法直接進行多筆資料的寫入，針對下方的範例進行解釋：

- 和上方範例不同的地方是，可以看到這邊是直接將讀出來的所有資料直接丟進 insert_many() 函式中
- 透過 inserted_ids 取得全部插入資料的 id
- 印出全部 id

**insert_many() 方法只接收 List[dictionary] 型態的資料，亦即要將全部的 dictionary 包進一個 list 當中**

```python
import os
import json
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient


# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 讀取 datas 目錄下的 1 月份資料
file = BASE_DIR / "datas" / "臺中市111年1月10大易肇事路口.json"

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(
    database=database,
    name="day004"
)
# 插入多筆資料
with open(file=file, encoding="utf-8") as f:
    datas = json.load(fp=f)
    
    inserted_ids = collection.insert_many(
        documents=datas
    ).inserted_ids
    
    print(inserted_ids)


client.close()
```

下圖中可以看到 Collection 內資料數量變為 11 筆 (本次插入 10 筆 + 上方範例插入 1 筆)
![插入多筆資料結果](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_004/img/%E6%8F%92%E5%85%A5%E5%A4%9A%E7%AD%86%E8%B3%87%E6%96%99%E7%B5%90%E6%9E%9C.jpg?raw=true)

## 三、Write Concern 寫入安全機制

在 mongodb 當中，提供了寫入安全機制，對應到 pymongo 當中的 insert_one 以及 insert_many 方法當中也都有對應的參數可以進行控制

下方圖片為正常 mongodb 在寫資料的過程

![一般寫入流程](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_004/img/%E4%B8%80%E8%88%AC%E5%AF%AB%E5%85%A5%E6%B5%81%E7%A8%8B.jpg?raw=true)

而在安全機制下寫入資料的流程則會將準備要寫入的資料存放到 mongodb 的 memory 中並建立副本，然後才將資料寫入到資料庫，
一旦資料在寫入的過程中有發生錯誤，可以即時的恢復，以達到確實的安全寫入機制

![安全機制寫入流程](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_004/img/%E5%AE%89%E5%85%A8%E6%A9%9F%E5%88%B6%E5%AF%AB%E5%85%A5%E6%B5%81%E7%A8%8B.jpg?raw=true)

下方利用 insert_one 的方式進行 demo

**由於 write_concern 是屬於 collection 的 property 級別參數，因此需於實體化 Collection 時就進行設定**

```python
import os
import json
from pathlib import Path
from dotenv import load_dotenv
from pymongo import WriteConcern
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 讀取 datas 目錄下的 1 月份資料
file = BASE_DIR / "datas" / "臺中市111年1月10大易肇事路口.json"

# 設定 write concern
write_concern = WriteConcern(
    w=2, j=True, wtimeout=1000
)

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(
    database=database,
    name="day004",
    write_concern=write_concern
)

# 插入單筆資料
with open(file=file, encoding="utf-8") as f:
    datas = json.load(fp=f)

    inserted_id = collection.insert_one(
        document=datas[0]
    ).inserted_id

client.close()
```

## 四、WriteConcern 參數說明
- w：指定寫入副本的數量，預設值為 1，要特別注意資料會等到副本全部建立後才會進行寫入，因此要留意副本建立數量
- j：設定為 True 會保證資料在真正寫入資料庫內後才會將資料進行 return，例如將 inserted_id 進行回傳
- wtimeout：設定副本寫入的超時時間，若超過將會拋出 WriteConcernTimeout 錯誤