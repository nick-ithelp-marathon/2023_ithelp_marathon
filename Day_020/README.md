# 玩轉 Python 與 MongoDB_Day20_delete 刪除資料

昨天我們把資料更新介紹完了，今天會來介紹如何刪除資料

> 語法：collection.delete_one({搜尋條件})

下方附上範例

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="demo")
collection = Collection(database=database, name="user")

inserted_id = collection.insert_one({"name": "Nick", "age": 20, "weight": 80}).inserted_id
data = collection.find_one({"_id": inserted_id})
print(data)

collection.delete_one({"_id": inserted_id})
data = collection.find_one({"_id": inserted_id})
print(data)

client.close()
```

下方的結果當中我們可以看到印出的第一筆資料有插入成功，而進行刪除後再次透過同個 _id 進行查詢則印出來的資料會是 None

![delete](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_020/img/delete.png?raw=true)