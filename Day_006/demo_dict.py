from enum import Enum
from pydantic import BaseModel


class LogLevel(Enum):
    DEBUG = "debug"
    INFO = "info"
    WARNING = "warning"
    ERROR = "error"
    CRITICAL = "critical"


class LogModel(BaseModel):
    level: LogLevel
    message: str

    class Config:
        use_enum_values = True


log = LogModel(level=LogLevel.WARNING, message="This is warning log.")
print(log.dict())
