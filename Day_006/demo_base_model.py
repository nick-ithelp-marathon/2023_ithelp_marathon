from typing import Optional
from datetime import datetime
from pydantic import BaseModel


class UserModel(BaseModel):
    name: str
    birthday: datetime
    email: Optional[str]
    created_time: datetime = datetime.now()


# 正常寫入資料
user = UserModel(
    name="nick",
    birthday=datetime.now(),
    email="nickchen1998@gmail.com"
)

print("------正常寫入資料------")
print(user)

# 缺少 Optional
user = UserModel(
    name="nick",
    birthday=datetime.now()
)
print("------缺少 Optional------")
print(user)

print("------缺少非 Optional------")
try:
    user = UserModel(
        birthday=datetime.now(),
        email="nickchen1998@gmail.com"
    )
except Exception as ex:
    print(ex.__class__.__name__)
