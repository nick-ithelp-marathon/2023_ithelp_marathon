from enum import Enum
from pydantic import BaseModel


class LogLevel(Enum):
    DEBUG = "debug"
    INFO = "info"
    WARNING = "warning"
    ERROR = "error"
    CRITICAL = "critical"


class LogModel(BaseModel):
    level: LogLevel
    message: str

    class Config:
        use_enum_values = True


log_list = [
    LogModel(level=LogLevel.DEBUG, message="This is debug level log."),
    LogModel(level=LogLevel.INFO, message="This is info level log.")
]

for log in log_list:
    if log.level == LogLevel.INFO.value:
        print(log.message)
