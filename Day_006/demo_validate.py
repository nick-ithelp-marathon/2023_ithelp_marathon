from typing import Optional
from datetime import datetime
from pydantic import BaseModel, validator, ValidationError


class UserModel(BaseModel):
    name: str
    birthday: datetime
    email: Optional[str]
    created_time: datetime = datetime.now()

    @validator("birthday")
    def over_18(cls, birthday: datetime):
        if datetime.now().year - birthday.year < 18:
            raise ValueError("年紀必須大於 18 歲")
        return birthday


print("----大於 18 歲----")
user = UserModel(
    name="nick", birthday=datetime(year=1999, month=1, day=1)
)
print(user)


print("----小於 18 歲----")
try:
    user = UserModel(
        name="nick", birthday=datetime(year=2023, month=1, day=1)
    )
    print(user)
except ValidationError as ex:
    print(ex)
