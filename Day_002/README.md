# 玩轉 Python 與 MongoDB_Day02_MongoDB Atlas

在開始介紹 Mongo 相關的語法之前，我們先來介紹一個由 MongoDB 公司提供的線上資料庫服務

## 一、MongoDB Atlas 簡介

MongoDB Atlas 是由 MongoDB 直接進行營運的一個線上資料庫平台，使用者可以在此平台上透過 AWS、GCP、AZURE 等各大提供商進行
MongoDB 的架設，
並且可以根據需求選擇自行控制權限、安全設定、資料備份等相關功能

在之後的教學文章中，筆者所使用的資料庫皆會透過連線的方式使用 MongoDB Atlas 進行程式的開發與 Mongo 語法的教學，下面會帶大家熟悉如何在
MongoDB Atlas 上架設資料庫以及利用 pymongo 進行連線

## 二、建立 MongoDB Atlas 組織與專案

在 MongoDB 上會需要建立一個組織，而每個組織底下可以有多個專案 (Project)，而每個專案底下可以有多個資料庫，而免費的只能有一個，
因此在建立資料庫之前我們必須先建立組織與專案

- 首先我們先造訪 [MongoDB Atlas 登入頁面](https://account.mongodb.com/account/login?signedOut=true)，並選擇一個你想要的登入方式進行登入

  ![MongoDB Atlas 登入頁面](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/MongoDB%20Atlas%20%E7%99%BB%E5%85%A5%E9%A0%81%E9%9D%A2.jpg?raw=true)

- 成功登入後會跳轉至此頁面，系統會要求你先建立一個組織，請點選 "Create an Organization"

  ![建立組織](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E5%BB%BA%E7%AB%8B%E7%B5%84%E7%B9%94.jpg?raw=true)

- 輸入組織名稱並選擇左邊的方案後，拉到最下方點選 "Next" 按鈕

  ![輸入組織名稱](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E8%BC%B8%E5%85%A5%E7%B5%84%E7%B9%94%E5%90%8D%E7%A8%B1.jpg?raw=true)

- 接下來會跳轉到加入使用者到你的組織的頁面，可以直接點選 "Create Organization" 選項

  ![確認建立組織](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E7%A2%BA%E8%AA%8D%E5%BB%BA%E7%AB%8B%E7%B5%84%E7%B9%94.jpg?raw=true)

- 接下來會跳轉至 Project 管理頁面，點選右上角的 "New Project" 選項

  ![新計畫](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E6%96%B0%E8%A8%88%E7%95%AB.jpg?raw=true)

- 輸入計畫名稱後並點選 "Next"

  ![輸入計畫名稱](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E8%BC%B8%E5%85%A5%E8%A8%88%E7%95%AB%E5%90%8D%E7%A8%B1.jpg?raw=true)

- 點選 "Create Project" 選項

  ![確認建立計畫](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E7%A2%BA%E8%AA%8D%E5%BB%BA%E7%AB%8B%E8%A8%88%E7%95%AB.jpg?raw=true)

## 三、建立資料庫

- 建立好專案後，預設會跳到此頁面，此時請點選 "Build a Database"

  ![建立資料庫](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E5%BB%BA%E7%AB%8B%E8%B3%87%E6%96%99%E5%BA%AB.jpg?raw=true)

- 接下來請選擇資料庫的方案，國家的部分原則上離你個國家越近連線的速度會越快，選擇完成後請點選 "Create"

  ![資料庫方案選擇](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E8%B3%87%E6%96%99%E5%BA%AB%E6%96%B9%E6%A1%88%E9%81%B8%E6%93%87.jpg?raw=true)
  ![資料庫方案選擇2](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E8%B3%87%E6%96%99%E5%BA%AB%E6%96%B9%E6%A1%88%E9%81%B8%E6%93%872.jpg?raw=true)

- 接下來會跳轉到設定安全性的部分，上方請先替這個資料庫建立一個使用者，並點選 "Create"

  ![建立資料庫使用者](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E5%BB%BA%E7%AB%8B%E8%B3%87%E6%96%99%E5%BA%AB%E4%BD%BF%E7%94%A8%E8%80%85.jpg?raw=true)

- 往下滑會需要設定有哪個 ip 可以連到這個資料庫，可以透過選擇 "Add My Current IP Address" 選項來自動加入目前的 IP，也可以設定
  0.0.0.0 對所有位置進行開放

  ![設定 IP 白名單](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E8%A8%AD%E5%AE%9A%20IP%20%E7%99%BD%E5%90%8D%E5%96%AE.jpg?raw=true)

- 設定完成後畫面會自動跳轉回專案首頁，這時候就可以看到剛剛建立好的資料庫出現了

## 四、取得連線資訊

- 點選 "Connect" 選項來取得連線資訊

  ![取得連線資訊](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E5%8F%96%E5%BE%97%E9%80%A3%E7%B7%9A%E8%B3%87%E8%A8%8A.jpg?raw=true)

- 接下來會需要選擇連線的方式，由於我們要使用程式進行連線，請選擇第一個選項

  ![選擇連線方式](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E9%81%B8%E6%93%87%E9%80%A3%E7%B7%9A%E6%96%B9%E5%BC%8F.jpg?raw=true)

- 接下來會跳轉到資料庫連線資訊的頁面，可以透過選擇程式語言的版本來提示你該安裝哪個版本的套件，在同個頁面中也可以看到資料庫的連線資訊

  ![資料庫連線資訊](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E8%B3%87%E6%96%99%E5%BA%AB%E9%80%A3%E7%B7%9A%E8%B3%87%E8%A8%8A.jpg?raw=true)

## 五、使用 Pymongo 與資料庫取得連線

在上方的表單中我們可以看到有一個 "View full code sample" 選項，點開後可以看到官方提供的建立連線的小程式碼，我們可以複製下來並執行看看

下方範例為複製官方程式碼後進行修改，筆者將連線資訊存放在 .env 檔案當中，並透過 python-dotenv 套件進行讀取

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.mongo_client import MongoClient

base_dir = Path(__file__).parent.parent / ".env"
load_dotenv(str(base_dir))

client = MongoClient(os.getenv("MONGODB_ATLAS_URL"))

try:
    client.admin.command('ping')
    print("Pinged your deployment. You successfully connected to MongoDB!")
except Exception as e:
    print(e)
```

可以看到透過上方的程式碼有成功執行並取得連線，過程中如果連線失敗，有可能是因為 IP 位置不對，在練習的時候我們可以先暫時將可連線的
IP 設定為 0.0.0.0
來讓大家都可以取得連線，如果是在正式環境當中，請不要使用此方法進行白名單的設定，也請不要在有設定 0.0.0.0 的資料庫當中存放任何機敏資訊

![執行結果](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_002/img/%E5%9F%B7%E8%A1%8C%E7%B5%90%E6%9E%9C.jpg?raw=true)