import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.mongo_client import MongoClient

base_dir = Path(__file__).parent.parent / ".env"
load_dotenv(str(base_dir))

client = MongoClient(os.getenv("MONGODB_ATLAS_URL"))

try:
    client.admin.command('ping')
    print("Pinged your deployment. You successfully connected to MongoDB!")
except Exception as e:
    print(e)
