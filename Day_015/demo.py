import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from pprint import pprint

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 取得所有 datas 檔案路徑下的所有檔案
file_names = []
for root, dirs, files in os.walk(BASE_DIR / "datas"):
    for file in files:
        file_path = os.path.join(root, file)
        file_names.append(file_path)

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="HighRiskIntersection")
intersection_collection = Collection(database=database, name="Intersection")
statistics_collection = Collection(database=database, name="Statistics")

# 依照檔案路徑將所有的資料打開並寫入資料庫
# for file in file_names:
#     with open(file=file, encoding="utf-8") as f:
#         datas = json.load(fp=f)
#
#         for data in datas:
#             # 建立 Statistics 物件
#             statistics = Statistics(
#                 A1_amount=data.get("A1", 0),
#                 A2_amount=data.get("A2件數", 0),
#                 A2_injury=data.get("A2受傷", 0),
#                 A3_amount=data.get("A3", 0),
#                 total_amount=data.get("總件數", 0)
#             )
#             statistics_inserted_id = statistics_collection.insert_one(statistics.dict()).inserted_id
#
#             # 建立 high_risk_intersection 物件
#             high_risk_intersection = HighRiskIntersection(
#                 year=2022,
#                 month=int(re.findall(r'(\d+)月', file)[0]),
#                 rank=data.get("編號"),
#                 jurisdiction=data.get("轄區分局"),
#                 intersection=data.get("路口名稱"),
#                 accident_time_interval=data.get("發生時間"),
#                 cause=data.get("主要肇因"),
#                 city="台中市",
#                 statistics_id=statistics_inserted_id
#             )
#             intersection_collection.insert_one(high_risk_intersection.dict())

data = intersection_collection.aggregate([
    {
        "$lookup": {
            "from": "Statistics",
            "localField": "statistics_id",
            "foreignField": "_id",
            "as": "statistic"
        }
    }
])
pprint(list(data))

client.close()
