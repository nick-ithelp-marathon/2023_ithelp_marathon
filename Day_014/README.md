# 玩轉 Python 與 MongoDB_Day14_聚合表達式

今天要介紹的是 expr，他可以讓我們在不使用 aggregate 的情況下進行一些簡易的表達式操作，節省我們編寫程式碼的時間

> 語法：{ $expr: { <expression> } }

下方範例中可以看到，我們透過表達式印出 A3 事故數量大於 A2 事故數量的所有資料，並且將路口名稱以及統計資訊印出來，
方便進行範例展示

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from pprint import pprint

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="HighRiskIntersection")
collection = Collection(database=database, name="Intersection")

print("---- demo expr ----")
result = collection.find(
    {"$expr": {"$gte": ["$statistics.A3_amount", "$statistics.A2_amount"]}},
    {"intersection": 1, "statistics": 1},
    limit=5
)
pprint(list(result))

client.close()
```

下圖中可以看到我們抓出來的資料全部都符合我們表達式中的條件

![expr 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_014/img/expr%20%E6%88%AA%E5%9C%96.jpg?raw=true)