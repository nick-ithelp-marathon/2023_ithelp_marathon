import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from pprint import pprint

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="HighRiskIntersection")
collection = Collection(database=database, name="Intersection")

print("---- demo expr ----")
result = collection.find(
    {"$expr": {"$gte": ["$statistics.A3_amount", "$statistics.A2_amount"]}},
    {"intersection": 1, "statistics": 1},
    limit=5
)
pprint(list(result))

client.close()
