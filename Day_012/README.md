# 玩轉 Python 與 MongoDB_Day12_進階查詢邏輯

前天我們解釋了一些常用的邏輯閘，例如：and、or 等等，我們也學到了可以把多個條件同時放進這些邏輯閘中的 list，
然而當今天你只想針對同個欄位做多個數值的判斷的話，使用這個方法會降低很多速度，今天要介紹的 in、nin 可以協助你在針對一個欄位做查詢時，
加速你的查詢速度，今天同樣會用到之前插入的資料集以及資料模型，忘記模型的人可以參考
[這個連結](https://github.com/nickchen1998/2023_ithelp_marathon/blob/main/Day_007/schema.py)

今天主要會教學的內容如下：

- $in 查詢條件
- $nin 查詢條件

## 一、$in

> 語法：collection.find_one({“欄位名稱”: {"$in": ['數值', '數值', ...]}})

這個語法可以協助我們針對指定的欄位做多個數值的配對，他會配對所有有出現在串列當中的數值，只要有符合的就會將資料回傳回來。

```python
# demo in
print("---- demo in ----")
result = collection.find(
    {"jurisdiction": {"$in": ["第四分局", "第六分局"]}},
    {"jurisdiction": 1},
    limit=5
)
pprint(list(result))
```

這個範例我們把查找資料的條件設定成分局名稱是第四分局或是第六分局，並且只把分局這個欄位顯示出來，且取資料前五筆，
可以看到下圖中的資料都是第四以及第六分局。

![in 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_012/img/in%20%E6%88%AA%E5%9C%96.jpg?raw=true)

## 二、$nin

> 語法：collection.find_one({“欄位名稱”: {"$nin": ['數值', '數值', ...]}})

基本上這個語法就是 in 語法的反面，會配對指定欄位中，任何不在串列中的資料，下方直接附上範例

```python
print("---- demo nin ----")
result = collection.find(
    {"jurisdiction": {"$nin": ["第四分局", "第六分局"]}},
    {"jurisdiction": 1},
    limit=5
)
pprint(list(result))
```

可以看到執行結果當中找到的資料分局名稱都不是第四以及第六分局

![nin 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_012/img/nin%20%E6%88%AA%E5%9C%96.jpg?raw=true)
