import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from pprint import pprint

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

# demo in
# print("---- demo in ----")
# result = collection.find(
#     {"jurisdiction": {"$in": ["第四分局", "第六分局"]}},
#     {"jurisdiction": 1},
#     limit=5
# )
# pprint(list(result))

# demo nin
print("---- demo nin ----")
result = collection.find(
    {"jurisdiction": {"$nin": ["第四分局", "第六分局"]}},
    {"jurisdiction": 1},
    limit=5
)
pprint(list(result))

client.close()
