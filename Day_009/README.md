# 玩轉 Python 與 MongoDB_Day09_分頁技術與排序

今天我們要來教學該如何進行分頁技術與排序，同樣會用到之前插入的資料集以及資料模型，忘記模型的人可以參考
[這個連結](https://github.com/nickchen1998/2023_ithelp_marathon/blob/main/Day_007/schema.py)

今天主要會教學的內容如下：

- 分頁 skip & limit
- 排序 sort

## 一、分頁技術介紹

### (一)、Limit

> 語法格式：collection.find({}).limit(<int>)

由於 MongoDB 在查詢資料的時候也會有只能回傳 16MB 大小的資料限制，並且通常為了加速查詢速度，
我們可以透過在 find 語法後呼叫 limit() 方法來限制每次要取得的資料筆數，下方附上範例

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from schema import HighRiskIntersectionInDB
from pprint import pprint

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

datas = collection.find({"jurisdiction": "第四分局"}).limit(5)
high_risk_intersections = [HighRiskIntersectionInDB(**data) for data in datas]

print(f"共取得 {len(high_risk_intersections)} 筆資料")
pprint(high_risk_intersections)

client.close()
```

可以看到我們在剛剛的範例中限制只要取出五筆資料筆數並印出取得的資料，下圖中也可以看到執行的結果確實是只有取出五筆資料

![limit 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_009/img/limit.jpg?raw=true)

### (二)、skip

> 語法格式：collection.find({}).skip(<int>)

剛才有提到 limit 是限制取得資料的比數，而 skip 則是會用來控制要跳過資料的比數，通常會搭配著 limit 來做使用，
讓程式可以依照一定的數量逐筆將全部的資料取出，下方附上程式範例

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from schema import HighRiskIntersectionInDB
from pprint import pprint

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

datas = collection.find({"jurisdiction": "第四分局"}).limit(2)
high_risk_intersections_1 = [HighRiskIntersectionInDB(**data) for data in datas]

print(f"---- 尚未使用 skip 取得的資料----")
pprint(high_risk_intersections_1)

datas = collection.find({"jurisdiction": "第四分局"}).limit(2).skip(2)
high_risk_intersections_2 = [HighRiskIntersectionInDB(**data) for data in datas]

print(f"---- 使用 skip 取得的資料----")
pprint(high_risk_intersections_2)

print("---- 驗證取得的兩筆資料是否相等 ----")
print(high_risk_intersections_1 == high_risk_intersections_2)

client.close()
```

在範例當中我們可以看到第一次在取得資料的時候只有使用 limit 取得兩筆資料，而在第二次取得資料的時候就有搭配 skip 來將前兩筆資料跳過，
因此可以看到將兩次取得的資料印出後的 id 部分是不一樣的，並且針對兩次取得的資料進行判斷是否相等，得到的結果也是不相等

![skip 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_009/img/skip.jpg?raw=true)

## 二、排序技術介紹

> 語法格式：collection.find({}).sort(<欄位>, <排序方式>)

> 排序方式物件路徑：from pymongo import ASCENDING, DESCENDING

> 補充說明：ASCENDING 為升冪，DESCENDING 為降冪

在 Mongodb 當中我們同樣可以透過排序來整理我們取得的資料，下方附上範例

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo import ASCENDING
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from schema import HighRiskIntersectionInDB
from pprint import pprint

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

datas = collection.find({"jurisdiction": "第四分局"}).limit(5).sort("month", ASCENDING)
high_risk_intersections = [HighRiskIntersectionInDB(**data) for data in datas]

print(f"共取得 {len(high_risk_intersections)} 筆資料")
pprint(high_risk_intersections)

client.close()
```

可以看到範例當中我們有在 find 的部分簡單給予一個條件，並且透過 sort 來進行排序，
在下圖的執行解果當中我們可以看到 month 的部分有成功按照升冪的方式進行排序

![sort 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_009/img/sort.jpg?raw=true)

## 三、結合使用

前面提到使用分頁技術以及排序技術的使用方式都是透過呼叫 find 底下對應的方法來進行，
然而在撰寫程式的過程中也經常會出現需要將這些設定做為參數傳遞，因此我們可以直接把剛剛提到的 limit、skip、sort 直接作為 kwargs 傳遞近 find 方法中進行使用下方為使用的語法格式

> 語法格式：collection.find({}, limit=<int>, skip=<int>, sort=[(<欄位>, <排序方式>)])

下方附上結和使用的範例

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from pymongo import DESCENDING
from pprint import pprint

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

datas = collection.find(
    {"jurisdiction": "第四分局"},
    {"month": 1, "jurisdiction": 1, "intersection": 1},
    limit=3,
    sort=[("month", DESCENDING)]
)
pprint(list(datas))

client.close()
```

可以看到上面的範例除了設定條件之外，也設定要取出的欄位，同時也設定了 limit 以及 sort 來進行資料筆數限制以及排序，
下圖為執行後的結果

![combine 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_009/img/combine.jpg?raw=true)