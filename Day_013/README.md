# 玩轉 Python 與 MongoDB_Day13_正規表達式

今天我們要來介紹一下正規表達式該怎麼與 MongoDB 來做查詢，同時也會針對正規表達式做一些簡單的介紹以及範例

## 一、正規表達式介紹

正則表達式（Regular Expressions），簡稱 regex 或 regexp，是一種強大的字串處理工具，用於搜索、匹配、替換和驗證文字資料。正則表達式由一個模式（pattern）組成，這個模式描述了我們想要匹配的文字的特定格式。以下是正則表達式的一些常見用途：

1. **搜尋（Search）**：使用正則表達式可以在文字中搜尋特定的模式，例如查找電子郵件地址、URL 或關鍵字。

2. **匹配（Match）**：確認文字是否符合特定的格式或模式，例如檢查日期、時間或身份證號碼的有效性。

3. **替換（Replace）**：將文字中的一部分替換為新的內容，例如將所有數字替換為*處理中*。

4. **分割（Split）**：將文字按特定的模式分割成多個部分，例如將句子分割成單詞。

5. **驗證（Validate）**：確保輸入的資料符合預期的格式，例如檢查密碼是否包含特定的字符。

以下是一些 Python 中使用正則表達式的基本範例：

```python
import re

# 搜尋文字中的數字
text = "The price of the product is $25."
result = re.search(r'\d+', text)
if result:
    print("Found:", result.group())  # 輸出: Found: 25

# 匹配電子郵件地址
email = "my.email@example.com"
if re.match(r'\w+@\w+\.\w+', email):
    print("Valid email address")

# 替換所有元音字母為 '*'
text = "Hello, World!"
new_text = re.sub(r'[aeiouAEIOU]', '*', text)
print(new_text)  # 輸出: H*ll*, W*rld!

# 分割文字為單詞
sentence = "This is a sample sentence."
words = re.split(r'\s', sentence)
print(words)  # 輸出: ['This', 'is', 'a', 'sample', 'sentence.']

# 驗證日期格式 (YYYY-MM-DD)
date = "2023-09-15"
if re.match(r'\d{4}-\d{2}-\d{2}', date):
    print("Valid date")
```

這邊附上正規表達式的線上編輯器，可以參考 [這個網址](https://www.regextester.com/)，我們可以藉由這些工具先進行初步的編輯，才不會每次要測試都對資料庫進行一次請求，造成過多的負擔

## 二、$regex 查詢條件

> 語法：collection.find({"$regex": "pattern"})

下方附上本次的範例，可以看到範例當中我們針對 Intersection 欄位中進行搜尋，而正規表達式的條件則設定為 ”臺灣大道” 且前後都必須要有文字，因此前後都有街上 ".*" 條件，表示前後都至少要有一個以上（包含）的文字

```python
# demo regex
print("---- demo regex ----")
result = collection.find(
    {"intersection": {"$regex": ".*臺灣大道.*"}},
    {"intersection": 1},
    limit=5
)
pprint(list(result))
```

下方附上查詢結果

![搜尋結果](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_013/img/%E6%90%9C%E5%B0%8B%E7%B5%90%E6%9E%9C.jpg?raw=true)