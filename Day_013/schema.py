from pydantic import BaseModel, Field
from datetime import datetime
from bson.objectid import ObjectId


class Statistics(BaseModel):
    A1_amount: int
    A2_amount: int
    A2_injury: int  # A2 受傷
    A3_amount: int
    total_amount: int


class HighRiskIntersection(BaseModel):
    year: int
    month: int

    rank: int  # 排名
    jurisdiction: str  # 轄區
    intersection: str  # 路口

    statistics: Statistics

    accident_time_interval: str  # 發生時間區間 (24 小時制)
    cause: str  # 主要肇因

    city: str

    created_time: datetime = datetime.now()


class HighRiskIntersectionInDB(HighRiskIntersection):
    id: ObjectId = Field(alias="_id")

    class Config:
        arbitrary_types_allowed = True
