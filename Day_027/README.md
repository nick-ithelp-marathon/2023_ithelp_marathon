# 玩轉 Python 與 MongoDB_Day27_Relational Migrator

今天我們要來介紹 MongoDB 在 7.0 版本以後新推出的資料庫遷移工具，他可以協助我們把關聯式資料庫的資料無痛遷移到 MongoDB 當中，
讓我們不需要煩惱轉移資料的問題，也可以節省開發這些功能的時間，非常方便！

工具的下載網址可以參考 [這個連結](https://www.mongodb.com/try/download/relational-migrator)，
有需要的話記得選擇自己的作業系統，並且要確保你的電腦可以連上指定的關聯式資料庫以及指定的 MongoDB


## 一、簡介

Relational Migrator 可以透過 GUI 的方式協助我們進行資料的轉移，並且可以生成轉移資料的程式碼，節省我們開發轉移資料腳本的時間，
目前可以生成的程式碼有 C#、JavaScript，未來可能會支援更多。

至於目前支援的關聯式資料庫種類有四種，分別是 MySQL、Oracle、SQL Server 以及 PostgreSQL，下方附上本次進行資料轉移的
SQLAlchemy 資料模型，資料的部分則是會沿用開頭提到的政府資料平台。


範例中 HighRiskIntersection 與 Statistics 為一對一的關聯，資料的部分已經有透過 SQLAlchemy 寫入到 mysql，
稍後我們會透過下方的步驟將 mysql 當中的資料轉移到 mongodb 當中。
```python
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship


# 創建基礎的SQLAlchemy模型
Base = declarative_base()


class Statistics(Base):
    __tablename__ = 'statistics'

    id = Column(Integer, primary_key=True, index=True)
    A1_amount = Column(Integer)
    A2_amount = Column(Integer)
    A2_injury = Column(Integer)
    A3_amount = Column(Integer)
    total_amount = Column(Integer)
    created_time = Column(DateTime, default=func.now())


class HighRiskIntersection(Base):
    __tablename__ = 'high_risk_intersection'

    id = Column(Integer, primary_key=True, index=True)
    year = Column(Integer)
    month = Column(Integer)
    rank = Column(Integer)
    jurisdiction = Column(String(100))
    intersection = Column(String(100))
    accident_time_interval = Column(String(100))
    cause = Column(String(100))
    city = Column(String(100))
    created_time = Column(DateTime, default=func.now())

    # 建立和Statistics模型的關聯
    statistics_id = Column(Integer, ForeignKey('statistics.id'), unique=True)
    statistics = relationship("Statistics")
```

## 二、操作步驟

- 先下載 Relational Migrator 並進行安裝
- 安裝完成後開啟應用程式會看到下方的預設畫面，請選擇第一個選項 "Connect database"

  ![預設畫面](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_027/img/%E9%A0%90%E8%A8%AD%E7%95%AB%E9%9D%A2.png?raw=true)

- 接著就會進入資料庫連線步驟，這邊的連線指的是與關聯式資料庫的連線，我們可以勾選右上方的 "Enter URI manually" 選項透過網址建立連線，並輸入使用者以及密碼

  ![建立關聯式資料庫連線](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_027/img/%E5%BB%BA%E7%AB%8B%E9%97%9C%E8%81%AF%E5%BC%8F%E8%B3%87%E6%96%99%E5%BA%AB%E9%80%A3%E7%B7%9A.png?raw=true)

- 接著選擇要進行轉移的資料表

  ![選擇資料表](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_027/img/%E9%81%B8%E6%93%87%E8%B3%87%E6%96%99%E8%A1%A8.png?raw=true)

- 接著選擇遷移方式，最上面可以選擇資料表名稱的命名方法，這邊選擇同原本的，而 mapping 的方式則選擇第二點，使用建議的 mongo schema

  ![選擇遷移方式](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_027/img/%E9%81%B8%E6%93%87%E9%81%B7%E7%A7%BB%E6%96%B9%E5%BC%8F.png?raw=true)

- 最後幫這個遷移的任務命名
- 命名完成後畫面會跳轉到查看 mapping 的頁面，這邊可以進行欄位的編輯，這邊我們確認完後沒問題可以點選上方的 "Data Migration" 選項進行遷移

  ![編輯資料欄位](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_027/img/%E7%B7%A8%E8%BC%AF%E8%B3%87%E6%96%99%E6%AC%84%E4%BD%8D.png?raw=true)

- 最後就可以看到他自動建立了一個遷移任務並且開始遷移

  ![遷移完成](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_027/img/%E9%81%B7%E7%A7%BB%E5%AE%8C%E6%88%90.png?raw=true)

- 下方附上 Mongo Compass 中顯示的遷移結果

  ![遷移結果](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_027/img/%E9%81%B7%E7%A7%BB%E7%B5%90%E6%9E%9C.png?raw=true)