# 玩轉 Python 與 MongoDB_Day03_資料表架構簡介

今天我們會介紹在資料在 MongoDB 裡面的架構會以怎麼樣的方式進行儲存，並且會和 SQL 的觀念做比對，同時也會介紹 Mongo Compass
這個工具

## 一、Mongo Compass

Mongo Compass 是 Mongo 公司所開發的一個可以瀏覽 MongoDB 的圖形化介面，下方為安裝的方式

- 前往 [Mongo Compass 官網](https://www.mongodb.com/products/compass) 並點選 "Download Now" 選項

- 左方選項選擇 "MongoDB Compass (GUI)" 選項，並確認版本後點選下載 (可使用預設)

  ![選擇 MongoCompass 版本](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_003/img/%E9%81%B8%E6%93%87%20Mongo%20Compass%20%E7%89%88%E6%9C%AC.jpg?raw=true)

- 直接點選下載下來的檔案就會自動幫你做安裝了

- 打開後就可以看到一個介面，請在紅色框框處輸入昨天取得的資料庫連線資訊

  ![MongoCompass 連線](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_003/img/Mongo%20Compass%20%E9%80%A3%E7%B7%9A.jpg?raw=true)

- 按下 "Save & Connect" 後會跳出視窗請你幫這個連線設定一些別名及標籤顏色，輸入後再次點選 "Save & Connect"

  ![設定別名](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_003/img/%E8%A8%AD%E5%AE%9A%E5%88%A5%E5%90%8D.jpg?raw=true)

- 下方韋連線成功的畫面

  ![連線成功](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_003/img/%E9%80%A3%E7%B7%9A%E6%88%90%E5%8A%9F.jpg?raw=true)

## 二、Mongo 與 SQL 觀念對照

| Mongo      | SQL      |
|------------|----------|
| Database   | Database |
| Collection | Table    |
| Document   | Raw Data |

## 三、建立資料表與插入一筆新的資料

- 點選 "+" 開始建立 Database
  
  ![建立資料庫](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_003/img/%E5%BB%BA%E7%AB%8B%E8%B3%87%E6%96%99%E5%BA%AB.jpg?raw=true)

- 接著會跳出一個表單，這裡我們可以在建立一個 Database 並且預先建立一個 Collection

  ![設定資料庫名稱](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_003/img/%E8%A8%AD%E5%AE%9A%E8%B3%87%E6%96%99%E5%BA%AB%E5%90%8D%E7%A8%B1.jpg?raw=true)

- 設定好後會跳轉到首頁，可以看到左方出現我們剛剛設定的 Database 以及 Collection，這時候我們可以點選 "ADD DATA" 來新增一筆資料
  
  ![ADD DATA](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_003/img/ADD%20DATA.jpg?raw=true) 

- 表單自動生成的 "_id" 欄位無須理會，其為 MongoDB 內部記錄每筆 Document 的編號，類似於 SQL 會自動往上增加的流水號可用於查詢
- 將要插入的資料以 JSON 格式的方式進行插入，填寫好後點選 "insert" 送出

  ![插入資料](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_003/img/%E6%8F%92%E5%85%A5%E8%B3%87%E6%96%99.jpg?raw=true)

- 插入成功後會自動跳轉到該 Collection 的首頁，即可看到剛剛插入的資料

  ![插入成功](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_003/img/%E6%8F%92%E5%85%A5%E6%88%90%E5%8A%9F.jpg?raw=true)

## 四、結論

1. Database 底下可以有很多個 Collection，如同 SQL 這邊也可以有很多 Table 一樣
2. 可以看到 Mongo 原則上是對於插入的資料除了型別之外沒有限制，基本是給什麼寫什麼
3. MongoDB 會為每一筆插入的 Document 自動生成一個 "_id" 的欄位，其類似於 SQL 流水號觀念