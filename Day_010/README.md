# 玩轉 Python 與 MongoDB_Day10_基本條件查詢邏輯

今天我們要來教學基本的 Mongo 邏輯語法，同樣會用到之前插入的資料集以及資料模型，忘記模型的人可以參考
[這個連結](https://github.com/nickchen1998/2023_ithelp_marathon/blob/main/Day_007/schema.py)

今天主要會教學的內容如下：

- $and 查詢條件
- $or 查詢條件
- $nor 查詢條件

## 一、$and 查詢條件

> 語法格式：collection.find({"$and":[{"欄位1": "value1"}, {}, {}......]})

這個運算條件會協助我們查詢同時符合在串列中的條件的資料，比起在 {} 裡面直接輸入兩個條件，使用 $and 運算符號會在查詢時更加的快速，下方附上範例

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from schema import HighRiskIntersectionInDB

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

datas = collection.find(
    {"$and": [
        {"jurisdiction": "第四分局"},
        {"intersection": "(南屯區)五權西路與環中路口"}
    ]}
)

for data in datas:
    print(HighRiskIntersectionInDB(**data))

client.close()
```

可以看到我們在剛剛的範例中指定分局以及路口，印出的結果也的確都是符合這個分局以及路口的資料

![and 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_010/img/and.jpg?raw=true)

## 二、$or 查詢條件

> 語法格式：collection.find({"$or":[{"欄位1": "value1"}, {}, {}......]})

這個條件會協助我們查詢符合在串列中任何一個條件的資料，下方附上範例

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from schema import HighRiskIntersectionInDB

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

datas = collection.find(
    {"$or": [
        {"jurisdiction": "第四分局"},
        {"jurisdiction": "第六分局"}
    ]}
).limit(5)

for data in datas:
    print(HighRiskIntersectionInDB(**data))

client.close()
```

可以看到範例當中設定了分局必須要是第四或是第六分局，因此可以看到下方執行結果只會出現第四以及第六分局的資料

![or 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_010/img/or.jpg?raw=true)

## 三、$nor 查詢條件

> 語法格式：collection.find({"$nor":[{"欄位1": "value1"}, {}, {}......]})

這個條件和 $or 相反，會配對不符合串列當中任何一個條件的資料，下方附上範例

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from schema import HighRiskIntersectionInDB

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

datas = collection.find(
    {"$nor": [
        {"jurisdiction": "第四分局"},
        {"jurisdiction": "第六分局"}
    ]}
).limit(5)

for data in datas:
    print(HighRiskIntersectionInDB(**data))

client.close()
```

可以看到範例當中設定的條件是非第四以及第六分局，因此在下方的執行結果當中也可以看到並沒有出現第四集第六分局的相關資料

![nor 截圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_010/img/nor.jpg?raw=true)