# 玩轉 Python 與 MongoDB_Day01_內容簡介
每天的專案會同步到 github 上，可以前往 [這個網址](https://github.com/nickchen1998/2023_ithelp_marathron)
如果對於專案有興趣或是想討論一些問題，歡迎留言 OR 來信討論，信箱為：nickchen1998@gmail.com

## 一、內容簡介
本次鐵人賽撰寫的目標為 python 與 mongodb，製作過程與內容預計的時程如下，本次內容除了會教學基本的 MongoDB 的資料操作之外，
還有關如何建立索引、資料庫權限、測試以及如何利用物件導向的方式來進行資料操作

| 主題               | 日程          |
|:-----------------|:------------|
| 簡介               | day 01      |
| 環境架設與 mongodb 介紹 | day 02 ~ 03 |
| 資料寫入             | day 04 ~ 05 |
| 資料結構與 Pydantic   | day 06 ~ 07 |
| 資料查詢             | day 08 ~ 16 |
| 資料關聯             | day 17      |
| LookUp           | day 18      |
| Aggregation 操作   | day 19 ~ 21 |
| 資料修改、刪除          | day 22 ~ 23 |
| 索引               | day 24 ~ 25 |
| 物件導向與 Pymongo    | day 26 ~ 27 |
| 資料庫權限            | day 28      |
| MongoMock 與測試    | day 29      |
| 結尾與系統資訊          | day 30      |

## 二、資料集選用

本次進行範例演練的資料皆來自 [政府公開資料](https://data.gov.tw/)，並統一下載成 JSON 檔案存放於 gitlab 專案目錄當中，
需要資料的人可以到 [這個網址](https://github.com/nickchen1998/2023_ithelp_marathron/tree/main/datas) 進行下載，
下方詳列出各個資料集來源：

- 臺中市111年1月10大易肇事路口：[https://data.gov.tw/dataset/152141](https://data.gov.tw/dataset/152141)
- 臺中市111年2月10大易肇事路口：[https://data.gov.tw/dataset/152362](https://data.gov.tw/dataset/152362)
- 臺中市111年2月10大易肇事路口：[https://data.gov.tw/dataset/155742](https://data.gov.tw/dataset/155742)
- 臺中市111年5月10大易肇事路口：[https://data.gov.tw/dataset/156465](https://data.gov.tw/dataset/156465)
- 臺中市111年6月10大易肇事路口：[https://data.gov.tw/dataset/158015](https://data.gov.tw/dataset/158015)
- 臺中市111年8月10大易肇事路口：[https://data.gov.tw/dataset/159066](https://data.gov.tw/dataset/159066)
- 臺中市111年9月10大易肇事路口：[https://data.gov.tw/dataset/160220](https://data.gov.tw/dataset/160220)
- 臺中市111年11月10大易肇事路口：[https://data.gov.tw/dataset/160551](https://data.gov.tw/dataset/160551)
- 臺中市111年12月10大易肇事路口：[https://data.gov.tw/dataset/162187](https://data.gov.tw/dataset/162187)

其他有使用到的資料集同樣會放在 datas 目錄下並於當日文章附上詳細網址