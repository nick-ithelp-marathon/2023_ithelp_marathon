import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient

author_list = [{"name": "Nick"}, {"name": "Andy"}]
book_list = [{"name": "book1"}, {"name": "book2"}]

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="demo")
author_collection = Collection(database=database, name="author")
book_collection = Collection(database=database, name="book")
author_book_relation_collection = Collection(database=database, name="author_book_relation")

# 寫入資料
author_collection.insert_many(author_list)
book_collection.insert_many(book_list)

for author in author_collection.find():
    for book in book_collection.find():
        author_book_relation_collection.insert_one({
            "author_id": author.get("_id"),
            "book_id": book.get("_id")
        })

client.close()
