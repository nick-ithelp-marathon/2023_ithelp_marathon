# 玩轉 Python 與 MongoDB_Day16_多對多關係

今天要來介紹在 MongoDB 當中該如何進行多對多關聯的建立，這邊我們會仿造關聯式資料庫的方式，使用第三張表來進行儲存多對多的關聯，
這樣的好處是在建立關聯的時候不需要針對主表進行修改，只需要透過第三張表來進行，下方附上本次的關聯圖，另外針對多對多關聯的查詢，
由於會使用到較多的 aggregate 的功能，我們會在明天的文章當中進行介紹

![資料關聯圖](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_016/img/%E8%B3%87%E6%96%99%E9%97%9C%E8%81%AF%E5%9C%96.png?raw=true)

下方附上範例程式，在這個程式當中，我們先將 author 以及 book 透過 insert_many 進行寫入，接著再透過迴圈的方式，替我們的資料建立關聯
，並將 _id 寫入記錄關聯的第三張表

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient

author_list = [{"name": "Nick"}, {"name": "Andy"}]
book_list = [{"name": "book1"}, {"name": "book2"}]

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="demo")
author_collection = Collection(database=database, name="author")
book_collection = Collection(database=database, name="book")
author_book_relation_collection = Collection(database=database, name="author_book_relation")

# 寫入資料
author_collection.insert_many(author_list)
book_collection.insert_many(book_list)

for author in author_collection.find():
    for book in book_collection.find():
        author_book_relation_collection.insert_one({
            "author_id": author.get("_id"),
            "book_id": book.get("_id")
        })

client.close()
```

下方的三張圖中可以看到我們成功將資料分別寫入到 author、book 以及 author_book_relation

- author
    
    ![author](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_016/img/author.jpg?raw=true)

- book

    ![book](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_016/img/book.jpg?raw=true)

- author_book_relation

    ![author_book_relation](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_016/img/author_book_relation.jpg?raw=true)
