import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from schema import HighRiskIntersection

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

datas = collection.find({"jurisdiction": "第四分局"})
print("----回傳 generator----")
print(f"型態為：{type(datas)}")
print(datas)

high_risk_intersection = [HighRiskIntersection(**data) for data in datas]
print("----轉換為物件----")
print(f"型態為：{type(high_risk_intersection)}")
print(high_risk_intersection)

client.close()
