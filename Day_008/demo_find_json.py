import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from schema import HighRiskIntersection

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

# 查找結構化資料
data = collection.find_one({"statistics.A2_amount": 5})
high_risk_intersection = HighRiskIntersection(**data)
print(high_risk_intersection)

client.close()
