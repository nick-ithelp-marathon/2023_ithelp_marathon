# 玩轉 Python 與 MongoDB_Day08_基本資料查詢

今天我們要來教學該如何進行基本的資料查詢，同樣會用到之前插入的資料集以及資料模型，忘記模型的人可以參考
[這個連結](https://github.com/nickchen1998/2023_ithelp_marathon/blob/main/Day_007/schema.py)

今天主要會教學的內容如下：

- 查找單筆資料
- 查找多筆資料
- 指定欄位查詢
- 查找結構化資料

## 一、查找單筆資料

> 語法格式：collection.find_one({"欄位名稱": "value"})

我們可以透過呼叫 collection 底下的 find_one 進行查詢，其接收的第一個參數為查詢條件，預設為空的 dictionary 代表無條件，
若有需要條件的話可以用 `{"欄位名稱": "value"}` 的方式進行

find_one 方法回傳的值為一個 dictionary，我們可以直接將回傳的資料用 kwargs 的方式直接打包進資料模型中做使用，下方附上程式碼範例

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from schema import HighRiskIntersection

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

# 查找單筆資料
data = collection.find_one({"jurisdiction": "第四分局"})
print("----回傳 dictionary----")
print(f"型態為：{type(data)}")
print(data)

# 將資料轉換為物件做使用
high_risk_intersection = HighRiskIntersection(**data)
print("----轉換為物件----")
print(f"型態為：{type(high_risk_intersection)}")
print(high_risk_intersection)

client.close()
```

下圖中可以看到我們成功透過給予條件 `{"jurisdiction": "第四分局"}` 查出分局名稱為 "第四分局" 的第一筆資料

**注意：查出來的資料是沒有經過任何排序的，就是單純從資料庫取得第一筆資料，排序相關功能會放到後面幾天的文章中介紹**

![find_one](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_008/img/find_one.jpg?raw=true)

## 二、查找多筆資料

> 語法格式：collection.find({"欄位名稱": "value"})

我們可以透過呼叫 find 方法來進行查詢，其設定條件的方式與 find_one 一樣，都是透過給予一個 dictionary 進行
與 find_one 的差別在於，其回傳值為一個 generator 的型態，必須透過迴圈或是 map 等方式對其進行跌代，下方附上範例

範例中我們可以看到我們將查找回來的 datas 使用 python comprehension 的方式對其進行跌代並建立一個串列，
串列中的每個元素都會是 HighRiskIntersection 的型態

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from schema import HighRiskIntersection

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

datas = collection.find({"jurisdiction": "第四分局"})
print("----回傳 generator----")
print(f"型態為：{type(datas)}")
print(datas)

high_risk_intersection_list = [HighRiskIntersection(**data) for data in datas]
print("----轉換為物件----")
print(f"型態為：{type(high_risk_intersection_list)}")
print(high_risk_intersection_list)

client.close()
```

下圖為執行成功後的範例，可以看到 datas 的型態為一個 pymongo 的 generator，而紅色方框內可以看到我們成功將每筆資料轉換為物件的型態

![find](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_008/img/find.jpg?raw=true)

## 三、指定欄位查詢

> 語法格式：collection.find({"欄位名稱": "value"}, {"欄位名稱": 1})

在 find 以及 find_one 方法中，我們可以透過設定第二個參數來進行指定欄位的查詢，其翻譯成 SQL 的概念大概為
`SELECT name FROM user` 就是只要把 user 這個資料表底下的 name 欄位全部抓出來即可，下方附上範例

範例中可以看到，我們將想要顯示的欄位透過 key value 的方式進行設定，將想要顯示的欄位設定為 1，不想顯示的欄位也不需設定為 0
當今天有傳遞此參數進 find 或是 find_one 方法時，除了 id
預設一定會顯示之外，其他都不會顯示，**但如果不想顯示 _id 則必須透過設定 {"_id": 0} 來讓 _id 不會顯示**

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from pprint import pprint

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

datas = list(collection.find(
    {"jurisdiction": "第四分局"},
    {"jurisdiction": 1, "intersection": 1, "month": 1}
))

pprint(datas)

client.close()
```

下方圖片當中可以看到我們透過設定 `{"jurisdiction": 1, "intersection": 1, "month": 1}` 將分局名稱、路口、以及月份這三個欄位搭配
條件為 "第四分局" 將資料進行顯示的結果

![show_column](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_008/img/show_column.jpg?raw=true)

## 四、查找結構化資料

> 語法格式：collection.find_one({"欄位名稱.欄位名稱": "value"})

想要查詢 json 格式中的資料，我們只需要透過 . 的方式來進行，比方說我要查找 statistic 底下的 A2_amount 為 5 的資料，
語法為 `{"statistic.A2_amount": 5}`，這樣 mongo 就會動去幫我們搜尋 JSON 格式，下方附上程式碼範例

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from schema import HighRiskIntersection

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="2023_ithelp")
collection = Collection(database=database, name="HighRiskIntersection")

# 查找結構化資料
data = collection.find_one({"statistics.A2_amount": 5})
high_risk_intersection = HighRiskIntersection(**data)
print(high_risk_intersection)

client.close()
```