import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from pprint import pprint

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="demo")
author_collection = Collection(database=database, name="author")

datas = list(author_collection.aggregate([
    {
        "$match": {
            "name": "Nick"
        }
    },
    {
        "$lookup": {
            "from": "author_book_relation",
            "localField": "_id",
            "foreignField": "author_id",
            "as": "author_book_relation"
        }
    },
    {
        "$unwind": "$author_book_relation"
    },
    {
        "$lookup": {
            "from": "book",
            "localField": "author_book_relation.book_id",
            "foreignField": "_id",
            "as": "books"
        }
    },
    {
        "$unwind": "$books"
    },
    {
        "$group": {
            "_id": "$_id",
            "name": {"$first": "$name"},
            "books": {"$push": "$books.name"}
        }
    }
]))
pprint(datas)

client.close()
