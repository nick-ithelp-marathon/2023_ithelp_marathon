import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from crawlers import get_ptt_gossiping_paragraph
from parsers import generate_embedding
from schema import DBParagraph

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 取得 Hugging Face Token
hf_token = os.getenv("HUGGING_FACE_TOKEN")

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="PTTParagraph")
collection = Collection(database=database, name="Gossiping")

# 寫入資料
# for tmp in get_ptt_gossiping_paragraph():
#     collection.insert_one(tmp.dict())

# 計算 embedding
# for paragraph in collection.find():
#     paragraph = DBParagraph(**paragraph)
#     embedding = generate_embedding(text=paragraph.title)
#     collection.update_one(
#         {"_id": paragraph.id},
#         {"$set": {"title_embedding": embedding}}
#     )

query = "Bella"

results = collection.aggregate([
    {
        '$search': {
            "index": "title_embedding_index",
            "knnBeta": {
                "vector": generate_embedding(query),
                "k": 5,
                "path": "title_embedding"
            }
        }
    }
])

for tmp in results:
    tmp = DBParagraph(**tmp)
    print(f"文章ＩＤ：{str(tmp.id)}，文章標題：{tmp.title}")

client.close()
