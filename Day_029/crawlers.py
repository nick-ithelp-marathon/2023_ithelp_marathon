import requests
from schema import Paragraph
from bs4 import BeautifulSoup
from datetime import datetime


def get_ptt_gossiping_paragraph() -> Paragraph:
    headers = {"cookie": "over18=1"}
    url = "https://www.ptt.cc/bbs/Gossiping/index.html"

    for i in range(10):
        response = requests.get(url=url, headers=headers)
        paragraph_list_soup = BeautifulSoup(response.text, "lxml")
        previous_page = paragraph_list_soup.select_one(
            "#action-bar-container > div > div.btn-group.btn-group-paging > a:nth-child(2)"
        )

        for paragraph in paragraph_list_soup.find_all(name="div", attrs={"class": "r-ent"}):
            if paragraph.find("a"):
                paragraph_response = requests.get(
                    url=f"https://www.ptt.cc{paragraph.find('a').get('href')}",
                    headers=headers
                )
                paragraph_soup = BeautifulSoup(paragraph_response.text, "lxml")
                if post_time := paragraph_soup.select_one("#main-content > div:nth-child(4) > span.article-meta-value"):
                    post_time = datetime.strptime(post_time.get_text(), '%a %b %d %H:%M:%S %Y')

                content = paragraph_soup.find(
                    name="div", attrs={"id": "main-content"}).get_text()
                author = paragraph_soup.select_one(
                    "#main-content > div:nth-child(1) > span.article-meta-value"
                )
                title = paragraph_soup.select_one(
                    "#main-content > div:nth-child(3) > span.article-meta-value"
                )

                if content and title and author:
                    print(title.text)
                    yield Paragraph(
                        post_time=post_time,
                        content=content,
                        title=title.text,
                        author=author.text,
                        board="Gossiping",
                        created_time=datetime.now(),
                        url=f"https://www.ptt.cc{paragraph.find('a').get('href')}"
                    )

        url = f"https://www.ptt.cc{previous_page.get('href')}"
