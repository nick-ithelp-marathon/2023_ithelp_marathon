from bson.objectid import ObjectId
from pydantic import BaseModel, Field
from datetime import datetime
from typing import List


class Paragraph(BaseModel):
    url: str
    title: str = None
    author: str = None
    content: str = None
    board: str = None
    post_time: datetime = None
    created_time: datetime = None
    title_embedding: List[float] = None


class DBParagraph(Paragraph):
    id: ObjectId = Field(alias="_id")

    class Config:
        arbitrary_types_allowed = True
