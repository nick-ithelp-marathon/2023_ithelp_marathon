import os
import requests


def generate_embedding(text: str) -> list[float]:
    embedding_url = "https://api-inference.huggingface.co/pipeline/feature-extraction/sentence-transformers/all-MiniLM-L6-v2"
    response = requests.post(
        embedding_url,
        headers={"Authorization": f"Bearer {os.getenv('HUGGING_FACE_TOKEN')}"},
        json={"inputs": text})

    if response.status_code != 200:
        raise ValueError(f"Request failed with status code {response.status_code}: {response.text}")

    return response.json()
