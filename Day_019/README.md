# 玩轉 Python 與 MongoDB_Day19_update 更新資料

前面的日子我們把寫入資料以及查詢資料的常用方法都介紹完了，接下來兩天我們要來介紹如何進行資料的更新以及刪除

今天我們會針對更新資料的方式以及 set 符號來做介紹

> 語法：collection.update_one({搜尋條件}, {"$set": {"欄位": "數值""}})

下方附上範例

```python
import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="demo")
collection = Collection(database=database, name="user")

inserted_id = collection.insert_one({"name": "Nick", "age": 20, "weight": 80}).inserted_id
data = collection.find_one({"_id": inserted_id})
print(data)

collection.update_one({"_id": inserted_id}, {"$set": {"name": "Andy"}})
data = collection.find_one({"_id": inserted_id})
print(data)

client.close()
```

下方的結果當中我們可以看到印出的第一筆資料是插入用的，第二筆則是透過 _id 進行搜尋並進行 update 的結果，name 的欄位有成功被更換成 Andy

![update](https://github.com/nickchen1998/2023_ithelp_marathon_mongodb/blob/main/Day_019/img/update.png?raw=true)