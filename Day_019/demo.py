import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="demo")
collection = Collection(database=database, name="user")

inserted_id = collection.insert_one({"name": "Nick", "age": 20, "weight": 80}).inserted_id
data = collection.find_one({"_id": inserted_id})
print(data)

collection.update_one({"_id": inserted_id}, {"$set": {"name": "Andy"}})
data = collection.find_one({"_id": inserted_id})
print(data)

client.close()
