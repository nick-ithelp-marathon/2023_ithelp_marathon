import os
from pathlib import Path
from dotenv import load_dotenv
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo.mongo_client import MongoClient
from pprint import pprint

# 讀取 .env 取得連線資訊
BASE_DIR = Path(__file__).parent.parent
load_dotenv(str(BASE_DIR / ".env"))

# 取得所有 datas 檔案路徑下的所有檔案
file_names = []
for root, dirs, files in os.walk(BASE_DIR / "datas"):
    for file in files:
        file_path = os.path.join(root, file)
        file_names.append(file_path)

# 建立 client 並與 db、collection 進行連線
client = MongoClient(host=os.getenv("MONGODB_ATLAS_URL"))
database = Database(client=client, name="HighRiskIntersection")
intersection_collection = Collection(database=database, name="Intersection")


# 使用 aggregate 和 project 查询数据
pipeline = [
    {
        "$lookup": {
            "from": "Statistics",
            "localField": "statistics_id",
            "foreignField": "_id",
            "as": "statistics"
        }
    },
    {
        "$unwind": "$statistics"
    },
    {
        "$project": {
            "_id": 0,
            "year": 1,
            "month": 1,
            "rank": 1,
            "jurisdiction": 1,
            "intersection": 1,
            "accident_time_interval": 1,
            "cause": 1,
            "city": 1,
            "created_time": 1,
            "A1_amount": "$statistics.A1_amount",
            "A2_amount": "$statistics.A2_amount",
            "A2_injury": "$statistics.A2_injury",
            "A3_amount": "$statistics.A3_amount",
            "total_amount": "$statistics.total_amount"
        }
    },
    {
        "$limit": 1
    }
]

data = intersection_collection.aggregate(pipeline)

pprint(list(data))

client.close()
